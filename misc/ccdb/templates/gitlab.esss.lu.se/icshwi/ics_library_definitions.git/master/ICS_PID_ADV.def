###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  PLC Sample Code in VersionDog: ICS_LIBRARY_MASTER_PLC                                   ## 
##  CCDB device types: ICS_xxxxx                                                            ##  
##  EPICS HMI (Block Icons/Faceplates)@ GitLab. Projekt: Cryo / CryogenicsLibrary / CryoLib ##
##                                                                                          ##  
##                                 PID - PID Control                                        ##  
##                                                                                          ##
##                                                                                          ##
############################           Version: 1.2           ################################
# Author:	Miklos Boros 
# Date:		08-04-2010
# Version:  v1.2
# Changes:  Removed Forced mode, added Regulation ON/OFF
############################           Version: 1.1           ################################
# Author:	Miklos Boros / Johannes Kazantzidis
# Date:		13-12-2019
# Version:  v1.1
# Changes:  Added alarm signal "Discrepancy_Error"
############################           Version: 1.0           ################################ 
# Author:	Miklos Boros
# Date:		01-09-2019
# Version:  v1.0
# Changes:  First release in TestStand2
##############################################################################################


############################
#  STATUS BLOCK
############################ 
define_status_block()


#Operation modes
add_digital("OpMode_Auto",             PV_DESC="Operation Mode Auto",       PV_ONAM="True",                          PV_ZNAM="False")
add_digital("OpMode_Manual",           PV_DESC="Operation Mode Manual",     PV_ONAM="True",                          PV_ZNAM="False")

add_digital("Regulation",              PV_DESC="PID Function State",        PV_ONAM="ON",                            PV_ZNAM="OFF")
add_analog("PID_Color",                "INT",                               PV_DESC="BlockIcon color")


#PID states
add_analog("SelectedPV",               "INT",   ARCHIVE=True,                              PV_DESC="Selected Measurement")
add_analog("LMN",                      "REAL",  PV_PREC="2",    ARCHIVE=True,                              PV_DESC="Manipulated Value",             PV_EGU="%")
add_analog("LMN_P",                    "REAL",  PV_PREC="2",    ARCHIVE=True,                              PV_DESC="Manipulated Value P-Action")
add_analog("LMN_I",                    "REAL",  PV_PREC="2",    ARCHIVE=True,                              PV_DESC="Manipulated Value I-Action")
add_analog("LMN_D",                    "REAL",  PV_PREC="2",    ARCHIVE=True,                              PV_DESC="Manipulated Value D-Action")
add_analog("PID_DIF",                  "REAL",  PV_PREC="2",    ARCHIVE=True,                              PV_DESC="Actual PID Error/Difference")
add_analog("PV",                       "REAL",  PV_PREC="2",    ARCHIVE=True,                              PV_DESC="PID Process Value")
add_analog("MAN_SP",                   "REAL",  PV_PREC="2",    ARCHIVE=True,                              PV_DESC="PID Setpoint Value")
add_analog("PID_Cycle",                "INT",                               PV_DESC="PID cycle time",                PV_EGU="ms")
add_string("ProcValueName", 39,         PV_NAME="ProcValueName",  ARCHIVE=True,            PV_DESC="PV Name of the Process Value")
add_string("ProcValueEGU", 6,           PV_NAME="ProcValueEGU",  ARCHIVE=True,             PV_DESC="EGU of the Process Value")

add_string("Meas1_Name", 39,         PV_NAME="Meas1_Name",  ARCHIVE=True,            PV_DESC="1st Select Measurement")
add_string("Meas2_Name", 39,         PV_NAME="Meas2_Name",  ARCHIVE=True,            PV_DESC="2nd Select Measurement")
add_string("Meas3_Name", 39,         PV_NAME="Meas3_Name",  ARCHIVE=True,            PV_DESC="3rd Select Measurement")

#Inhibit signals (set by the PLC code, can't be changed by the OPI)
add_digital("Inhibit_Manual",          PV_DESC="Inhibit Manual Mode",       PV_ONAM="InhibitManual",                 PV_ZNAM="AllowManual")
add_digital("Inhibit_Lock",            PV_DESC="Inhibit Locking",           PV_ONAM="InhibitLocking",                PV_ZNAM="AllowLocking")

#Interlock signals
add_digital("MoveInterlock",  ARCHIVE=True,           PV_DESC="Move Interlock",            PV_ONAM="True",                          PV_ZNAM="False")
add_digital("GroupInterlock",          PV_DESC="Group Interlock",           PV_ONAM="True",                          PV_ZNAM="False")
add_string("InterlockMsg", 39,                       PV_NAME="InterlockMsg",               PV_DESC="Interlock Message")

#for OPI visualization
add_digital("EnableAutoBtn",           PV_DESC="Enable Auto Button",        PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnableManualBtn",         PV_DESC="Enable Manual Button",      PV_ONAM="True",                          PV_ZNAM="False")
add_digital("EnablePIDConf",           PV_DESC="Enable PID Configuration",  PV_ONAM="True",                          PV_ZNAM="False")
add_digital("LatchAlarm",              PV_DESC="Enable Alarm Latching",     PV_ONAM="True",                          PV_ZNAM="False")
add_digital("GroupAlarm",              PV_DESC="Group Alarm for OPI")

#Block Icon controls
add_digital("EnableBlkCtrl",           PV_DESC="Enable Block SP Button",     PV_ONAM="True",             PV_ZNAM="False")


#Locking mechanism
add_digital("DevLocked",               PV_DESC="Device locked",             PV_ONAM="True",                          PV_ZNAM="False")
add_analog("Faceplate_LockID",         "DINT",                              PV_DESC="Owner Lock ID")
add_analog("BlockIcon_LockID",         "DINT",                              PV_DESC="Guest Lock ID")


#Alarm signals
add_digital("LMN_HLIM",                                                     PV_ZNAM="True")
add_digital("LMN_LLIM",                                                     PV_ZNAM="True")
add_major_alarm("Input_Module_Error",  "HW Input Module Error",             PV_ZNAM="NominalState")
add_major_alarm("Output_Module_Error", "HW Output Module Error",            PV_ZNAM="NominalState")
add_major_alarm("Discrepancy_Error",   "Discrepancy_Error",                 PV_ZNAM="NominalState",                  PV_ONAM="Timeout")

#Warning signals
add_minor_alarm("ContDeviceInMan",     "ContDeviceInMan",                   PV_ZNAM="True")

#Feedbacks
add_analog("FB_Setpoint",              "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Setpoint from HMI (SP)")
add_analog("FB_Step",                  "REAL" ,  ARCHIVE=True,                             PV_DESC="FB Step value for Open/Close",  PV_EGU="%")
add_analog("FB_Manipulated",           "REAL" ,  ARCHIVE=True,                             PV_DESC="Force Manipulated value (AO)",  PV_EGU="%")

#Actual PID Profile
add_digital("Profile1",                                                     PV_ZNAM="True")
add_digital("Profile2",                                                     PV_ZNAM="True")
add_digital("Profile3",                                                     PV_ZNAM="True")

#Selected PID Profile
add_analog("FB_Gain",                  "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Proportional gain")
add_analog("FB_TI",                    "DINT" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Integration time",           PV_EGU="msec")
add_analog("FB_TD",                    "DINT" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Derivative action time",     PV_EGU="msec")
 
add_analog("FB_DEADB",                 "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM",              "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM",              "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Low limit for MV")

#PID Profile 1
add_analog("FB_Gain_1",                "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Proportional gain")
add_analog("FB_TI_1",                  "DINT" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Integration time",           PV_EGU="msec")
add_analog("FB_TD_1",                  "DINT" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Derivative action time",     PV_EGU="msec")

add_analog("FB_DEADB_1",               "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM_1",            "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM_1",            "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Low limit for MV")


#PID Profile 2
add_analog("FB_Gain_2",                "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Proportional gain")
add_analog("FB_TI_2",                  "DINT" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Integration time",           PV_EGU="msec")
add_analog("FB_TD_2",                  "DINT" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Derivative action time",     PV_EGU="msec")

add_analog("FB_DEADB_2",               "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM_2",            "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM_2",            "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Low limit for MV")

#PID Profile 3
add_analog("FB_Gain_3",                "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Proportional gain")
add_analog("FB_TI_3",                  "DINT" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Integration time",           PV_EGU="msec")
add_analog("FB_TD_3",                  "DINT" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Derivative action time",     PV_EGU="msec")

add_analog("FB_DEADB_3",               "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Deadband")
add_analog("FB_LMN_HLIM_3",            "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB High limit for MV")
add_analog("FB_LMN_LLIM_3",            "REAL" ,  ARCHIVE=" 1Hz",                             PV_DESC="FB Low limit for MV")


add_digital("FB_P_ActionState",        PV_DESC="Proportional Action State")
add_digital("FB_I_ActionState",        PV_DESC="Integral Action State")
add_digital("FB_D_ActionState",        PV_DESC="Differential Action State")

#Standby Synchroniztaion
add_digital("FB_SS_State",             PV_DESC="Standby Synchroniztaion State")
add_analog("FB_SS_Value",              "REAL" ,                             PV_DESC="Standby Synchroniztaion Value", PV_EGU="%")


############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",              PV_DESC="CMD: Manual Mode")

add_digital("Cmd_EnablePID_Conf",      PV_DESC="CMD: Enable PID configuration from OPI")
add_digital("Cmd_DisablePID_Conf",     PV_DESC="CMD: Disable PID configuration from OPI")

add_digital("Cmd_P_ON",                PV_DESC="CMD: Enable P-Action")
add_digital("Cmd_P_OFF",               PV_DESC="CMD: Disable P-Action")

add_digital("Cmd_Regu_ON",             PV_DESC="CMD: PID Function ON")
add_digital("Cmd_Regu_OFF",            PV_DESC="CMD: PID Function OFF")

add_digital("Cmd_Profile1",            PV_DESC="CMD: Select PID Profile 1")
add_digital("Cmd_Profile2",            PV_DESC="CMD: Select PID Profile 2")
add_digital("Cmd_Profile3",            PV_DESC="CMD: Select PID Profile 3")

add_digital("Cmd_I_ON",                PV_DESC="CMD: Enable I-Action")
add_digital("Cmd_I_OFF",               PV_DESC="CMD: Disable I-Action")

add_digital("Cmd_D_ON",                PV_DESC="CMD: Enable D-Action")
add_digital("Cmd_D_OFF",               PV_DESC="CMD: Disable D-Action")

add_digital("Cmd_AckAlarm",            PV_DESC="CMD: Acknowledge Alarm")

add_digital("Cmd_SelectPV1",           PV_DESC="CMD: Select Measurement 1")
add_digital("Cmd_SelectPV2",           PV_DESC="CMD: Select Measurement 2")
add_digital("Cmd_SelectPV3",           PV_DESC="CMD: Select Measurement 3")

add_digital("Cmd_ForceUnlock",         PV_DESC="CMD: Force Unlock Device")
add_digital("Cmd_DevLock",             PV_DESC="CMD: Lock Device")
add_digital("Cmd_DevUnlock",           PV_DESC="CMD: Unlock Device")

add_digital("Cmd_SelMeasDev1",         PV_DESC="CMD: Select Measured Device 1")
add_digital("Cmd_SelMeasDev2",         PV_DESC="CMD: Select Measured Device 2")
add_digital("Cmd_SelMeasDev3",         PV_DESC="CMD: Select Measured Device 3")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()
#PID Profile 1
add_analog("P_Gain_1",                 "REAL" ,                             PV_DESC="PID Proportional gain")
add_analog("P_TI_1",                   "DINT" ,                             PV_DESC="PID Integration time",          PV_EGU="msec")
add_analog("P_TD_1",                   "DINT" ,                             PV_DESC="PID Derivative action time",    PV_EGU="msec")

add_analog("P_DEADB_1",                "REAL" ,                             PV_DESC="PID Deadband")
add_analog("P_LMN_HLIM_1",             "REAL" ,                             PV_DESC="PID High limit for MV")
add_analog("P_LMN_LLIM_1",             "REAL" ,                             PV_DESC="PID Low limit for MV")

#PID Profile 2
add_analog("P_Gain_2",                 "REAL" ,                             PV_DESC="PID Proportional gain")
add_analog("P_TI_2",                   "DINT" ,                             PV_DESC="PID Integration time",          PV_EGU="msec")
add_analog("P_TD_2",                   "DINT" ,                             PV_DESC="PID Derivative action time",    PV_EGU="msec")

add_analog("P_DEADB_2",                "REAL" ,                             PV_DESC="PID Deadband")
add_analog("P_LMN_HLIM_2",             "REAL" ,                             PV_DESC="PID High limit for MV")
add_analog("P_LMN_LLIM_2",             "REAL" ,                             PV_DESC="PID Low limit for MV")

#PID Profile 3
add_analog("P_Gain_3",                 "REAL" ,                             PV_DESC="PID Proportional gain")
add_analog("P_TI_3",                   "DINT" ,                             PV_DESC="PID Integration time",          PV_EGU="msec")
add_analog("P_TD_3",                   "DINT" ,                             PV_DESC="PID Derivative action time",    PV_EGU="msec")

add_analog("P_DEADB_3",                "REAL" ,                             PV_DESC="PID Deadband")
add_analog("P_LMN_HLIM_3",             "REAL" ,                             PV_DESC="PID High limit for MV")
add_analog("P_LMN_LLIM_3",             "REAL" ,                             PV_DESC="PID Low limit for MV")


#Setpoint and Manipulated value from HMI
add_analog("P_Setpoint",               "REAL" ,                             PV_DESC="Setpoint from HMI (SP)",        PV_EGU="%")
add_analog("P_Manipulated",            "REAL" ,                             PV_DESC="Force Manipulated value (AO)",  PV_EGU="%")

#Step value when pressing Cmd_Open1Step or Cmd_Close1Step
add_analog("P_Step",                   "REAL" ,                             PV_DESC="Step value for open close",     PV_EGU="%")


#Locking mechanism
add_analog("P_Faceplate_LockID",       "DINT",                              PV_DESC="Device ID after Lock")
add_analog("P_BlockIcon_LockID",       "DINT",                              PV_DESC="Device ID after Blockicon Open")
